<?php

// +----------------------------------------------------------------------
// 个人通讯录
// +----------------------------------------------------------------------

namespace app\person\controller;
error_reporting(0);
use library\Controller;
use think\Db;


/**
 * 个人通讯录
 */
class Myphone extends Controller
{

    /**
     * 绑定当前数据表
     * @var string
     */
    protected $table = 'Phone';

    /**
     * 通讯录列表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index()
    {
        $this->title = '个人通讯录列表';
        $user = session('admin_user')['nickname'];
        $this->_query($this->table)->like('nickname,tel,mail,company')
        	 ->where(" adduser LIKE '%".$user."%' AND is_deleted=0 AND status=1 and id > 10000")
        	 ->equal('status')->order(' status ASC , id DESC')->page();
       	$this->assign("user" , $user);
    }

	/**
	 * 列表数据处理
	 * @param array $data
	 * @throws \Exception
	 */
	protected function _index_page_filter(&$data)
	{

	}

    protected function _page_filter(&$data){

    }



    /**
     * 添加
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add()
    {
        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }



    /**
     * 编辑
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit()
    {
    	$user = session('admin_user')['nickname'];

    	$id =intval($this->request->param('id'));

    	$rs =Db::table('Phone')->where('id',$id)->find();

    	if($rs['adduser'] != $user){
    		$this->error('没有权限修改！' );
    	}

        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 删除
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function remove()
    {
        $this->applyCsrfToken();
        $this->_delete($this->table);
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _form_filter(&$data)
    {
    	if ($this->request->isPost()) {

    		$post = $this->request->post();
    		$data['addtime'] = time();
    		$data['adduser'] = session('admin_user')['nickname'];
    	}
    }




}
