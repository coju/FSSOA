<?php

// +----------------------------------------------------------------------
// 请休假
// +----------------------------------------------------------------------

namespace app\person\controller;
error_reporting(0);
use library\Controller;
use think\Db;
use think\Request;
//update system_user set buxiu=zhiban+jiaban,yixiubuxiu = zhibanbuxiu+jiabanbuxiu
//创建buxiu yixiubuxiu


/**
 * 请休假
 */
class Holiday extends Controller
{

    /**
     * 绑定当前数据表
     * @var string
     */
    protected $table = 'Jiaqis';
    
    public $leixing = [''=>'-- 类型 --', 1=>'年休假',2=>'值班补休',3=>'加班补休',4=>'病假',5=>'看护假',
            6=>'事假',7=>'产假',8=>'探亲假',9=>'工伤假' ,10=>'婚假'
            ,11=>'哺乳假',12=>'丧假',13=>'其他'
        ];
	/*
    public $leixing = [''=>'-- 类型 --', 1=>'年休假',2=>'补休',4=>'病假',5=>'看护假',
            6=>'事假',7=>'产假',8=>'探亲假',9=>'工伤假' ,10=>'婚假'
            ,11=>'哺乳假',12=>'丧假',13=>'其他'
        ];
	*/

    public $leixing_yixiu =['1'=>'yxnianjia' , '2'=>'zhibanbuxiu' , '3'=>'jiabanbuxiu']; //已休编码

    public $leixing_no =['2'=>'zhiban' , '3'=>'jiaban' ]; //zhiban，jiaban标记

    public $leixing_jiaban =['jiaban'=>'加班' , 'zhiban'=>'值班' ]; //zhiban，jiaban标记

    public $status =['1'=>'<span class="layui-bg-green">已确认</span>' , '0'=>'<span class="layui-bg-blue">待审核</span','-1'=>'<span class="layui-bg-black">已撤销</span' , '99'=>'<span class="layui-bg-red">正在流转</span>'];

    protected $param;

    /**
     * 我的加班/值班
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function myduty()
    {

        $this->title = '我的加班/值班';
        
        $user = session('admin_user')['nickname'];
        if ($user !='管理员'){

                $this->_query($this->table)->like('name,datetype')
                 ->where(" name LIKE '%".$user."%' AND is_deleted=0 AND status !=1")
                 ->equal('status')->order(' otdate DESC , id DESC')->page();
        }else{
                $this->_query($this->table)->like('name#name,datetype')
                 ->where(" is_deleted=0 AND status !=1")
                 ->equal('status')->order(' otdate DESC , id DESC')->page();  
        }

    }




    /**
     * 我的休假
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index()
    {
        $this->title = '我的休假';

        $this->table ='UsersJiaqi';
        $user = session('admin_user')['nickname'];
        if ($user !='管理员'){
            $this->_query($this->table)->like('name#name,leixing,status')
            	 ->where(" name LIKE '%".$user."%' AND is_deleted=0 ")
            	 ->equal('leixing')->order('startdate DESC , id DESC')->page();
        }else{
            $this->_query($this->table)->like('name#name,leixing,status')
                 ->where(" is_deleted=0 ")
                 ->equal('leixing')->order('startdate DESC , id DESC')->page();
        }
        $this->assign('leixing' , $this->leixing);
    }




    /**
     * 我的年假
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function myyearholiday()
    {
        $this->title = '我的年假';
        //insert into users_nianjia (name,nianjia,yixiunianjia,years) select nickname, nianjia,yxnianjia,2018 from system_user 
        $this->table ='UsersNianjia';
        $user = session('admin_user')['nickname'];
        if ($user !='管理员'){
            $this->_query($this->table)->like('name#name , years')
            	 ->where(" name LIKE '%".$user."%' ")
				 ->equal('years')
            	 ->order('years DESC , id DESC')->page();
        }else{
            $this->_query($this->table)->like('name#name,years')
                 ->where(true)
				 ->equal('years')
                 ->order('years DESC , id DESC')->page();
        }
    }



    /**
     * 值班加班列表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function list()
    {
        $user = session('admin_user')['nickname'];
        $page =intval($this->request->param('page'));
        $name =$this->request->param('name');
        $datetype =$this->request->param('datetype');

        $this->table ='Jiaqis';
        if ($user !='管理员'){
        		/*
                $rs = $this->_query($this->table)->like('name,datetype')
                 ->where(" name LIKE '%".$user."%' AND is_deleted=0 AND status !=1")
                 ->equal('status')->order(' otdate DESC , id DESC')->page(100);
				*/
				$page = empty($page) ? 1 : $page;//获取当前页数
				$list = Db::table($this->table)
			    //->alias('a')
			    //->join('ProductsBrand b','a.BrandId=b.BrandId')
			    ->where(" name ='".$name."' AND is_deleted=0 AND status =0 AND datetype='".$datetype."'  ")
			    ->order(' otdate DESC , id DESC')
			    ->paginate(10)->each(function($item, $key){
				    //$item['nickname'] = 'think';
				    $item['datetype']              = $this->leixing_jiaban[$item['datetype']];
				    return $item;
				});

        }else{
        		/*
                $rs = $this->_query($this->table)->like('name#name,datetype')
                 ->where(" is_deleted=0 AND status !=1")
                 ->equal('status')->order(' otdate DESC , id DESC')->page(100);
                 */
				$page = empty($page) ? 1 : $page;//获取当前页数
				$list = Db::table($this->table)
			    //->alias('a')
			    //->join('ProductsBrand b','a.BrandId=b.BrandId')
			    ->where(" name LIKE '%".$name."%' AND is_deleted=0 AND status =0 AND datetype='".$datetype."' ")
			    ->order(' otdate DESC , id DESC')
			    ->paginate(10)->each(function($item, $key){
				    //$item['nickname'] = 'think';
				    $item['datetype']              = $this->leixing_jiaban[$item['datetype']];
				    return $item;
				});

        }
        

        #print_r($list);

		$page = $list->render();

		$this->assign("list",$list);
		$this->assign("page",$page);

        $this->_form($this->table, 'list');
    }

    /**
     * 撤销加班
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function changeJiaqi()
    {
        $id =intval($this->request->param('id'));
        $this->table ='Jiaqis';
        $rst =[];
        if($id <=0){
        	$rst['code'] =0;
        	$rst['msg'] ='error';
        	return ($rst);
        }
        Db::table('Jiaqis')->where("  id='".$id."'  ")->update( ['status'=>-1 ]);
    	$rst['code'] =1;
    	$rst['msg'] ='ok';
    	return ($rst);
    }





	/**
	 * 列表数据处理
	 * @param array $data
	 * @throws \Exception
	 */
	protected function _list_page_filter(&$data)
	{
        foreach ($data as &$vo) {
            $vo['datetype']              = $this->leixing_jiaban[$vo['datetype']];
            //$vo['admin_status_text']    = $this->status[$vo['status']];

        }
	}



    /**
     * 年假编辑
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit_yearholiday()
    {
        //$this->title = '我的年假';
        //insert into users_nianjia (name,nianjia,yixiunianjia,years) select nickname, nianjia,yxnianjia,2018 from system_user 
        $this->table ='UsersNianjia';
        $user = session('admin_user')['nickname'];
        $id =intval($this->request->param('id'));
        $rs =Db::name( $this->table )->where('id',$id)->find();

        if($this->request->isPost()){
            list($data1, $string) = [$this->request->post(), []];

    		$data1['update_time'] = time();

    		//更新
			Db::name( $this->table  )
			    ->where('id', $data1['id'])
			    ->strict(false)
			    ->data($data1)
			    ->update();

            $this->success('保存成功!', '/admin.html#/person/holiday/myyearholiday.html?spm=m-77-88-94');
        }



        //增加发文的表单如下-----------------------------------
        $list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
        $str ='';
        foreach ($list as &$value) {
            $str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
        }



        #echo $str;
        $this->assign('vo' , $rs);
        $this->assign('list' , $list);
        $this->assign('str' , $str);
        $this->_form($this->table, 'edit_yearholiday');
        
    }

    /**
     * 审批列表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function uncheckholiday()
    {
        $this->title = '审批请休假';

        $this->table ='UsersJiaqi';
        $user = session('admin_user')['nickname'];

        
        if ($user =='管理员' || $user =='陈伟光' ){
            $this->_query($this->table)->like('name#name,leixing,status')
                 ->where("  is_deleted=0 ")
                 ->equal('leixing')->order('startdate DESC , id DESC')->page();
        }else{

            $status =intval($this->request->param('status'));
            if ($status ==1) {
                $query =" AND a.status=1 AND a.allusers LIKE '%".$user."%' AND f.status=1 AND f.info!='' ";
            }elseif ($status ==0) {
                $query =" AND a.status=0 AND a.allusers LIKE '%".$user."%' AND f.status=1 AND f.info='' ";
            }else{
                $query =" AND a.status=-1 AND a.allusers LIKE '%".$user."%' ";
            }

            #echo $query;

            $this->_query($this->table)->like('a.name#name,a.leixing,a.status')
                ->field('a.*')
                ->alias('a')
                ->join('users_jiaqi_flow f','f.docid = a.id')
                 ->where(" a.is_deleted=0   " . $query)
                 ->group('f.docid')
                 ->equal('leixing')->order('a.startdate DESC , a.id DESC ')->page();
            $this->assign('leixing' , $this->leixing);
            $this->fetch('index.html');
                 
        }

    }
    #46cd0b84e1de36c9c281efd3bc98c9a6

    /**
     * 列表数据处理
     * @param array $data
     * @throws \Exception
     */
    protected function _uncheckholiday_page_filter(&$data)
    {
        $user = session('admin_user')['nickname'];
        foreach ($data as &$vo) {
            $vo['leixing_text']              = $this->leixing[$vo['leixing']];
            $tmp =Db::table('users_jiaqi_flow')->where(['username'=>$user , 'docid'=>$vo['id'] ])->find();
            if ($tmp['info'] !='' && $vo['status']  !=1) {
                $vo['status'] ='99';
            }
            
            $vo['admin_status_text']    = $this->status[$vo['status']];
        }
    }


	/**
	 * 列表数据处理
	 * @param array $data
	 * @throws \Exception
	 */
	protected function _index_page_filter(&$data)
	{
        foreach ($data as &$vo) {
            $vo['leixing_text']              = $this->leixing[$vo['leixing']];
            $vo['admin_status_text']    = $this->status[$vo['status']];

        }
	}

    protected function _page_filter(&$data){

    }

    /**
     * 添加请休假
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add_holiday()
    {
        $this->title = '申请休假';
        $this->isAddMode = '1';
        $this->table ='UsersJiaqi';
        if ($this->request->isPost()) {
            list($data1, $string) = [$this->request->post(), []];

            /*
            Array ( [j_id] => Array ( [0] => 308 [1] => 177 ) [j_name] => Array ( [0] => 林玮 [1] => 林玮 ) [j_otdate] => Array ( [0] => 2019-12-07 [1] => 2019-09-07 ) [j_datetype] => Array ( [0] => 值班 [1] => 值班 ) [j_buxiunum] => Array ( [0] => 3 [1] => 3 ) [j_yixiunum] => Array ( [0] => 0.0 [1] => 2.0 ) [j_days] => Array ( [0] => 2 [1] => 1 ) [shouwenwenyuan] => 林玮 [keshifuzeren] => 丁岳炼 [fenguanlingdao] => [remark] => , 2019-12-07值班 , 2019-09-07值班 [files] => [types] => 2 )
             */
            //$this->error('参数错误!'.$this->request->param('types') );
            $data1['status'] =0;

            $data1['leixing'] =$this->request->param('types');
            if( !is_numeric( $data1['days'] ) ||  $data1['name'] =='' || $data1['days'] <=0 ) 
            { 
                $this->error('参数错误!' );
            } 
            if ($data1['shouwenwenyuan'] =='') {
            	$this->error('请选择人事审批人!' );
            }

            $data1['create_time'] =time();
            $data1['docauthor'] =$data1['name'];

            if ($data1['docauthor'] =='' ) {
                $data1['docauthor'] = session('admin_user')['nickname'];
            }
            
            $data1['allusers'] = $data1['docauthor'].','.$data1['keshifuzeren'].','.$data1['fenguanlingdao'].','.session('admin_user')['nickname'].','.$data1['shouwenwenyuan'];

            $data1['wenhao'] =6;

            if($data1['leixing'] ==2 || $data1['leixing'] ==3){
            	$j_days =array_sum($data1['j_days']);
            	if ($j_days != $data1['days']) {
            		$this->error('数据有误，请核实!' );
            	}
            }

			Db::startTrans();
			try{

	            $ids =Db::name( $this->table )->strict(false)->insertGetId($data1);

	            //申请人发布
	            if($data1['docauthor'] !=''){
	                $this->Insertflow($data1['docauthor'] , $info ='发起申请' , $types='nigao' , $ids  , $flag = 0, 'holiday');
	            }
	            //人事
	            $this->Insertflow($data1['shouwenwenyuan'] , $info ='' , $types='shouwenwenyuan' , $ids , $flag = 1, 'holiday');

	            //科室负责人
	            $this->Insertflow($data1['keshifuzeren'] , $info ='' , $types='keshifuzeren' , $ids , $flag = 2, 'holiday');

	            //分管领导
	            $this->Insertflow($data1['fenguanlingdao'] , $info ='' , $types='fenguanlingdao' , $ids , $flag = 3, 'holiday');

	            //已休总数追加
	            $type =$data1['types'];
	            if($type =='1'){
	                $type ='yxnianjia';
					$years =date('Y' , time() );
					Db::name( 'UsersNianjia')->where( ['name'=>$data1['name'],'years'=>$years ])->setInc('yixiunianjia',  $data1['days'] ); // 已休的+1
					Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc($type,  $data1['days'] ); // 已休的+1
					
	            }else if($type ==2) {
	                $type ='zhibanbuxiu';
					Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc($type,  $data1['days'] ); // 已休的+1
					Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc('yixiubuxiu',  $data1['days'] ); // 已休的+1		
	            }else if($type ==3) {
	                $type ='jiabanbuxiu';
					Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc($type,  $data1['days'] ); // 已休的+1
					Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc('yixiubuxiu',  $data1['days'] ); // 已休的+1
	            }

	            $num = count($data1['j_id']);
	            if($num >0 ){
		            if($data1['leixing'] ==2 || $data1['leixing'] ==3){
		            	for ($i=0; $i < $num ; $i++) { 
		            		$rst['j_id'] = $data1['j_id'][$i];
		            		$rst['j_name'] = $data1['j_name'][$i];
		            		$rst['j_otdate'] = $data1['j_otdate'][$i];
		            		$rst['j_datetype'] = $data1['j_datetype'][$i];
		            		$rst['j_buxiunum'] = $data1['j_buxiunum'][$i];
		            		$rst['j_yixiunum'] = $data1['j_yixiunum'][$i];
		            		$rst['j_days'] = $data1['j_days'][$i];
		            		$rst['jiaqi_id'] = $ids;
		            		if ($rst['j_days'] == $rst['j_buxiunum'] -$rst['j_yixiunum']) {
		            			Db::table('jiaqis')->where('id', $rst['j_id'] )->update(['status'=>'-1']);
		            		}
		            		//更新加班/值班记录
		            		Db::table('jiaqis')->where('id', $rst['j_id'] )->setInc('yixiunum', $rst['j_days'] );
		            		$arr[] = $rst;
		            	}
		            	//插入流水明细
		            	Db::table( 'User_usedjiaqi' )->insertAll($arr);
		            }
	            }

			    Db::commit();    
			} catch (\Exception $e) {
			    // 回滚事务
			    Db::rollback();
			}

            $this->success('提交成功，等待审批!', '/admin.html#/person/holiday/index.html?spm=m-77-88-90');
        }

        //增加发文的表单如下-----------------------------------
        $list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
        $str ='';
        foreach ($list as &$value) {
            $str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
        }

        $rs =[];
        $this->assign('str' , $str);
        $this->assign('vo' , $rs);
        $this->assign('leixing' , $this->leixing);
        $this->assign('list' , $list);
        $this->_form($this->table, 'holidayform');
        
    }



    //新增的时候插入流程
    private function Insertflow($data , $info , $types ,$ids , $flag , $doctypes)
    {
        if($data ==''){
            return false;
        }
        $tmp1 = explode(',' , $data);
        $flow1 =[];
        if(is_array($tmp1)){
            foreach ($tmp1 as &$v) {
                if($v !=''){
                    array_push($flow1, [
                        'docid'     => $ids,
                        'username'  => $v,
                        'types'     => $types,
                        'info'      => $info,
                        'addtime'   => time(),
                        'files'     => '',
                        'flag'      => $flag,
                        'doctypes'  => $doctypes,
                    ]);
                }
            }
        }else{
            $flow1[] =[
                'docid'     => $ids,
                'username'  => $tmp1,
                'types'     => $types,
                'info'      => $info,
                'addtime'   => time(),
                'files'     => '',
                'flag'      => $flag,
                'doctypes'  => $doctypes,
            ];
        }
        if (!empty($flow1)) {
            Db::name('UsersJiaqiFlow')->insertAll($flow1);
        }
        return true;
    }


    /*获取值班/补休*/
    public function getdate()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param('name');
            $info = Db::table('System_User')->where(['nickname'=>$data ])->find();
            return $info;
        }
    }

    /*获取年假*/
    public function getyears()
    {
        if ($this->request->isPost()) {
        	$years =date('Y' , time());
            $data = $this->request->param('name');
            $info = Db::table('Users_Nianjia')->where(['name'=>$data,'years' =>$years ])->find();
            return $info;
        }
    }


    /**
     * 编辑请休假
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit_holiday()
    {
        $user = session('admin_user')['username'];

        $id =intval($this->request->param('id'));
        $this->table ='UsersJiaqi';
        $rs =Db::table('Users_Jiaqi')->where('id',$id)->find();
        

        if($rs['name'] != $user){
            //$this->error('没有权限修改！' );
        }

        if (stripos($rs['allusers'], session('admin_user')['nickname'] ) ===FALSE) {
            $this->error('权限不足，操作失败！' );
        }

        //我的流程
        $my_flow = Db::table('Users_Jiaqi_Flow')
                ->where( " username ='".session('admin_user.nickname')."' AND  docid='".$rs['id']."' AND status=1 AND doctypes='holiday' AND info ='' ")
                ->find();

        if($my_flow){
            //我之前的流程
            $my_flow_count = Db::table('Users_Jiaqi_Flow')
                ->where( "  docid='".$rs['id']."' AND status=1 AND doctypes='holiday'  AND info ='' AND flag <".$my_flow['flag']." ")
                ->count('id');
        }else{
            $my_flow_count =0;
        }

        $rs_flow = Db::table('Users_Jiaqi_Flow')
                ->where( "  username ='".session('admin_user.nickname')."' AND doctypes='holiday' AND docid='".$rs['id']."' AND status=1 AND info ='' ")
                ->count('id');

        //已经审批的意见列表
        $list_flow = Db::table('Users_Jiaqi_Flow')
                ->where( "  docid='".$rs['id']."' AND doctypes='holiday'  AND status=1 AND info !='' ")
                ->order('addtime ASC,id ASC')
                ->select();

        //值班/加班假期流水
        $jiaqi_flow = Db::table('User_Usedjiaqi')
                ->where( "  jiaqi_id='".$rs['id']."' ")
                ->select();

        //增加发文的表单如下-----------------------------------
        $list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
        $str ='';
        foreach ($list as &$value) {
            $str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
        }

        $keshifuzeren =$this->showUser($rs['keshifuzeren']);
        $fenguanlingdao =$this->showUser($rs['fenguanlingdao']);
        $shouwenwenyuan =$this->showUser($rs['shouwenwenyuan']);

        $this->assign('str' , $str);
        $this->assign('vo' , $rs);
        $this->assign('remark' , $rs['remark']);
        $this->assign('attaches' , $rs['files']);
        $this->assign('id' , $id);
        $this->assign('keshifuzeren' , $keshifuzeren);
        $this->assign('fenguanlingdao' , $fenguanlingdao);
        $this->assign('shouwenwenyuan' , $shouwenwenyuan);  
        $this->assign('leixing' , $this->leixing);

        $this->assign('my_flow_count' , $my_flow_count);
        $this->assign('rs_flow' ,       $rs_flow);
        $this->assign('list_flow' ,     $list_flow);
        $this->assign('jiaqi_flow' ,     $jiaqi_flow);
        $this->applyCsrfToken();
        $this->_form($this->table, 'holidayform');
    }

    private function showUser($data)
    {
        if($data !=''){
            $tmp1 = explode(',' , $data);
            $str1 ='';
            if(is_array($tmp1)){
                foreach ($tmp1 as $key => &$v) {
                    $str1 .="'".$v."',";
                }
            $str1 =rtrim($str1 , ',');
            }else{
                $str1 =$data;
            }
            
        }
        return $str1;
    }




    /**
     * 查看休假
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function viewdoc()
    {
        $user = session('admin_user')['username'];

        $id =intval($this->request->param('id'));
        $this->table ='UsersJiaqi';
        $rs =Db::table('Users_Jiaqi')->where('id',$id)->find();
        
        if($rs['name'] != $user){
            //$this->error('没有权限修改！' );
        }

        //已经审批的意见列表
        $list_flow = Db::table('Users_Jiaqi_Flow')
                ->where( "  docid='".$rs['id']."' AND doctypes='holiday'  AND status=1 AND info !='' ")
                ->order('addtime ASC,id ASC')
                ->select();


        //值班/加班假期流水
        $jiaqi_flow = Db::table('User_Usedjiaqi')
                ->where( "  jiaqi_id='".$rs['id']."' ")
                ->select();

        //增加发文的表单如下-----------------------------------
        $list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
        $str ='';
        foreach ($list as &$value) {
            $str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
        }

        $keshifuzeren =$this->showUser($rs['keshifuzeren']);
        $fenguanlingdao =$this->showUser($rs['fenguanlingdao']);
        $shouwenwenyuan =$this->showUser($rs['shouwenwenyuan']);



        $this->assign('str' , $str);
        $this->assign('vo' , $rs);
        $this->assign('remark' , $rs['remark']);
        $this->assign('attaches' , $rs['files']);
        $this->assign('id' , $id);
        $this->assign('keshifuzeren' , $keshifuzeren);
        $this->assign('fenguanlingdao' , $fenguanlingdao);
        $this->assign('shouwenwenyuan' , $shouwenwenyuan);  
        $this->assign('leixing' , $this->leixing);

        $this->assign('my_flow_count' , $my_flow_count);
        $this->assign('rs_flow' ,       $rs_flow);
        $this->assign('list_flow' ,     $list_flow);
        $this->assign('jiaqi_flow' ,     $jiaqi_flow);
        $this->applyCsrfToken();


        $this->_form($this->table, 'holidayform');
    }

    /**
     * 保存批示
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function saveinfo()
    {
        if ($this->request->isPost()) {
            $data =$this->request->param();
            $rs['info'] = $data['info'];
            $rs['docid'] =intval($data['docid']);

            if($rs['docid'] ==0){
                $res =[];
                $res['code']  ='0';
                $res['status']  ='error';
                $res['msg']  ='错误参数！';
                return $res;
            }
            $rs['username'] = session('admin_user.nickname');

            $rs['addtime'] =time();


            Db::table('users_jiaqi_flow')->where("  username ='".$rs['username']."' AND doctypes='holiday' AND docid='".$rs['docid']."' AND status=1  ")->update([  'info'=>$rs['info'],'addtime'=>$rs['addtime'] ]);
            //Db::table('Users_Jiaqi_Flow')->where("  username ='".$rs['username']."' ")->update([  'info'=>$rs['info'] ]);

            $rs_flow = Db::table('users_jiaqi_flow')
                ->where( "  docid='".$rs['docid']."' AND doctypes='holiday' AND status=1 AND info ='' ")
                ->count('id');
            if($rs_flow ==0){
                Db::table('users_jiaqi')->where("  id='".$rs['docid']."'  ")
                    ->update( ['status'=>1 ]);
            }


            $res =[];
            $res['code']  ='1';
            $res['status']  ='success';
            $res['msg']  ='保存成功！';
            return $res;
        }
    }




    /**
     * 添加加班/值班
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add()
    {
        $this->title = '添加加班/值班';
        //$this->isAddMode = '1';

        if($this->request->isPost()){
            list($data1, $string) = [$this->request->post(), []];
            $data1['create_time'] = time();

            $ids =Db::name("Jiaqis")->strict(false)->insertGetId($data1);

            Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc($data1['datetype'],  $data1['buxiunum'] ); // 已休的+1
            Db::name( 'SystemUser')->where( ['nickname'=>$data1['name'] ])->setInc('buxiu',  $data1['buxiunum'] ); // 补休总数+1

            $this->success('保存成功!', '/admin.html#/person/holiday/myduty.html?spm=m-77-88-89');

        }

        $list = Db::name('SystemUser')->field('id,username,nickname')->where(['status'=>'1'])->select();
        $str ='';
        foreach ($list as &$value) {
            $str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
        }

        #echo $str;
        $this->assign('list' , $list);
        $this->assign('str' , $str);
        $this->_form($this->table, 'form');
    }





    /**
     * 编辑加班/值班
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit()
    {
    	$user = session('admin_user')['username'];

    	$id =intval($this->request->param('id'));

    	$rs =Db::table('Jiaqis')->where('id',$id)->find();

		$old_type =$rs['datetype']; //旧的类型
		$old_kexiu =$rs['buxiunum']; //旧的天数
		$name = $rs['name']; //原来的名字

		$user_rs =Db::table('System_User')->where(['nickname'=>$name])->find();

    	if($rs['status'] ==1){
    		$this->error('被禁用，无法编辑！' );
    	}


    	if ($this->request->isPost()) {
    		list($data1, $string) = [$this->request->post(), []];
    		$data1['update_time'] = time();


    		$user_kexiu =$user_rs[$old_type]; //旧的可用天数
	    	if($data1['buxiunum'] > $user_kexiu && $old_type == $data1['datetype']) //类型一样时，比较天数,
	    	{
	    		$this->error('此人可休的假期只有'.$user_kexiu.'天，无法编辑'.$data1['buxiunum'].'天，请重新核准数据！' );
	    	}

    		//更新
			Db::name('Jiaqis')
			    ->where('id', $rs['id'])
			    ->strict(false)
			    ->data($data1)
			    ->update();

	    	Db::name( 'SystemUser')->where( ['nickname'=>$name ])->setDec($old_type,  $old_kexiu ); // 旧类型总数-旧的天数 撤销原来的
	    	//Db::name( 'SystemUser')->where( ['nickname'=>$name ])->setDec('buxiu',  $old_kexiu ); // 补休天数-$old_kexiu
	    	Db::name( 'SystemUser')->where( ['nickname'=>$name ])->setInc($data1['datetype'],  $data1['buxiunum'] ); // 新类型总数+ 新的天数 新增

	    	$this->success('更新成功!', '/admin.html#/person/holiday/myduty.html?spm=m-77-88-89');

    	}

        $list = Db::name('SystemUser')->field('id,username,nickname')->where(['status'=>'1'])->select();
        $str ='';
        foreach ($list as &$value) {
            $str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
        }

        $this->assign('list' , $list);
        $this->assign('user_rs' , $user_rs);
        $this->assign('str' , $str);
        $this->assign('vo' , $rs);
        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 撤销信息
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function forbid()
    {
    	$id =intval($this->request->param('id'));
    	$rs =Db::table('Jiaqis')->where('id',$id)->find();
    	if(!$rs){
    		$this->error('参数错误！' );
    	}

    	$name = $rs['name'];
    	$old_type =$rs['datetype'];
    	$old_kexiu =$rs['buxiunum'];
    	

    	$user_rs =Db::table('System_User')->where(['nickname'=>$name])->find();
    	$user_kexiu =$user_rs[$old_type];
    	if($old_kexiu > $user_kexiu)
    	{
    		$this->error('此人可休的假期只有'.$user_kexiu.'天，不足以撤销'.$old_kexiu.'天，请重新核准数据！' );
    	}

    	Db::name( 'SystemUser')->where( ['nickname'=>$name ])->setDec($old_type,  $old_kexiu ); // 总数-旧的天数
    	Db::name( 'SystemUser')->where( ['nickname'=>$name ])->setDec('buxiu',  $old_kexiu ); // 补休总数-旧的天数

        $this->_save($this->table, ['status' => '1' , 'update_time'=> time() ]);
    }

    /**
     * 恢复信息
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function resume()
    {

    	$id =intval($this->request->param('id'));
    	$rs =Db::table('Jiaqis')->where('id',$id)->find();
    	if(!$rs){
    		$this->error('参数错误！' );
    	}

    	$name = $rs['name'];
    	$old_type =$rs['datetype'];
    	$old_kexiu =$rs['buxiunum'];
    	
    	$user_rs =Db::table('System_User')->where(['nickname'=>$name])->find();
    	$user_kexiu =$user_rs[$old_type];

    	Db::name( 'SystemUser')->where( ['nickname'=>$name ])->setInc($old_type,  $old_kexiu ); // 已休的+1

        $this->_save($this->table, ['status' => '0', 'update_time'=> time()]);
    }

    /**
     * 撤销请休假
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function remove()
    {
    	$user = session('admin_user')['nickname'];
    	$id =intval($this->request->param('id'));
    	$rs =Db::table('users_jiaqi')->where('id',$id)->find();
    	if($rs['name'] != $user  ){
    		$this->error('只能撤销你本人的申请，参数错误！' );
    	}

    	if($rs['status'] ==1 ){
    		$this->error('已经审批，无法撤销！' );
    	}

    	$name = $rs['name'];

    	$user_rs =Db::table('System_User')->where(['nickname'=>$name])->find();

    	$rs_used =Db::table('user_usedjiaqi')->where('jiaqi_id',$rs['id'])->select();

		// 启动事务
		Db::startTrans();
		try{

	    	//1,2,3 年假，值班，加班
	    	if($rs['leixing'] <= 3){
				if($rs['leixing'] ==1){
					$years =date('Y', time());
					Db::name( 'UsersNianjia')->where( ['name'=>$name ,'years'=>$years ])->setDec('yixiunianjia',  $rs['days'] ); // 从已休的里面-天数
				}else{
					$leixing_yixiu =$this->leixing_yixiu[$rs['leixing']];
					// 从加班、值班已休的里面-天数	
					Db::name( 'SystemUser')->where( ['nickname'=>$name ] )->setDec($leixing_yixiu,  $rs['days'] ); 
					// 从总的已休的里面-天数
					Db::name( 'SystemUser')->where( ['nickname'=>$name ] )->setDec('yixiubuxiu',  $rs['days'] ); 
			    	if (count($rs_used) > 0 ) {
			    		foreach ($rs_used as $k => $v) {
			    			//撤销追减jiaqis的已休数据
			    			Db::table('jiaqis')->where('id',$v['j_id'])->update(['status'=>'0']);
			    			Db::table('jiaqis')->where('id',$v['j_id'])->setDec('yixiunum' , $v['j_days']);
			    			//删除已用记录
			    			Db::table('user_usedjiaqi')->where('id',$v['id'])->delete();
			    		}
			    	}
				}
	    		Db::table('users_jiaqi')->where('id',$id)->update(['keshifuzeren'=>'','fenguanlingdao'=>'','shouwenwenyuan'=>'','allusers'=>$name,  'days'=>0,'update_time'=>time(),'status'=>'-1' ]);

	    		Db::table('users_jiaqi_flow')->where('docid',$id)->delete();
	    	}else{
	    		Db::table('users_jiaqi')->where('id',$id)->update(['keshifuzeren'=>'','fenguanlingdao'=>'','shouwenwenyuan'=>'','allusers'=>$name,  'days'=>0,'update_time'=>time(),'status'=>'-1' ]);
	    		Db::table('users_jiaqi_flow')->where('docid',$id)->delete();
	    	}
	    	
		    Db::commit();    
		} catch (\Exception $e) {
		    // 回滚事务
		    Db::rollback();
		}
    	
    	$this->success('撤销成功' );

        //$this->_delete($this->table);
    }

    /**
     * 强制办结
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function state()
    {
    	$id =($this->request->param('id'));
    	$ids =explode(',' , $id);

    	if(count($ids) > 1){
    		$this->error('强制办结，只能一次选择一条记录！' );
    	}

    	$rs =Db::table('users_jiaqi')->where('id',$id)->find();
    	if ($rs['status'] !=0) {
    		$this->error('该记录已经办结或撤销，无法再强制办结！' );
    	}

    	$this->table ='UsersJiaqi';
    	$this->_save($this->table, ['status' => '1', 'update_time'=> time()]);
    }

    /**
     * 打印假条
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function printdoc()
    {
    	$id =intval($this->request->param('id'));
    	$rs =Db::table('users_jiaqi')->where('id',$id)->find();

    	if ($rs['status'] !=1) {
    		$this->error('还未办结确认，无法打印！' );
    	}

    	$flow =Db::table('users_jiaqi_flow')->where('docid',$id)->select();

    	$this->assign('vo' , $rs);
    	$this->assign('leixing' , $this->leixing);
    	$this->assign('flow' , $flow);

    	$this->fetch();
    }


    /**
     * 统计报表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */    
    public function report()
    {
        $this->title = '考勤统计';

    	$this->param  = $this->request->param();

        if (isset($this->param['start_date']) && !empty($this->param['start_date'])) {
            $param['start_date']                      = $this->param['start_date'];            
        }else{
        	$param['start_date']   = date('Y-m-d' ,time());
        }
        $this->assign('start_date', $param['start_date']);


        if (isset($this->param['end_date']) && !empty($this->param['end_date'])) {
            $param['end_date']                        = $this->param['end_date'];
        }else{
        	$param['end_date']   = date('Y-m-d' , mktime(0, 0, 0, date("m")+1, date("d"),   date("Y")) );
        }
        $this->assign('end_date', $param['end_date']);

        $list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
        $this->assign('list', $list);
        $this->fetch();
    }


    public function upload()
    {
        if($this->request->file('file')){
            $file       = $this->uploadfile('file','' ,[] , session('admin_user')['id']);
            if ($file) {
                echo json_encode($file);
            } else {
                echo json_encode($file);
            }
        }
    }


    public function uploadfile($name, $path = '', $validate = [], $user_id = 0)
    {
        $config = config('attchment');
        $file   = request()->file($name);
        if ($file) {
            $file_path = $config['path'] . $path;
            $file_url  = $config['url'] . $path;
            $validate  = array_merge($config['validate'], $validate);
            $info      = $file->validate($validate)->move($file_path);
            if ($info) {
                $file_info = [
                    'code'          => 0 ,
                    'user_id'       => $user_id,
                    'original_name' => $info->getInfo('name'),
                    'save_name'     => $info->getFilename(),
                    'save_path'     => str_replace("\\", "/",$file_path . $info->getSaveName()),
                    'extension'     => $info->getExtension(),
                    'mime'          => $info->getInfo('type'),
                    'size'          => $info->getSize(),
                    'md5'           => $info->hash('md5'),
                    'sha1'          => $info->hash(),
                    'url'           => str_replace("\\", "/",$file_url . $info->getSaveName())
                ];
                return $file_info;
            } else {
                return $file->getError();
            }
        } else {
            return '无法获取文件';
        }
        return false;
    }



    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _form_filter(&$data)
    {
    	if ($this->request->isPost()) {
    	}
    }





}
