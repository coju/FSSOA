<?php

// +----------------------------------------------------------------------
// 通讯录
// +----------------------------------------------------------------------

namespace app\person\controller;
error_reporting(0);
use library\Controller;
use think\Db;


/**
 * 电话通讯录
 */
class Phone extends Controller
{

    /**
     * 绑定当前数据表
     * @var string
     */
    protected $table = 'SystemUser';

    /**
     * 通讯录列表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index()
    {
        $this->title = '单位人员列表';
        $user = session('admin_user')['username'];
        $this->_query($this->table)->like('nickname,tel,mail')
        	 ->where(" is_deleted=0 AND status=1 and id > 10000")
        	 ->equal('status')->order(' id ASC , status ASC  ' )->page();
       	$this->assign("user" , $user);
    }

	/**
	 * 列表数据处理
	 * @param array $data
	 * @throws \Exception
	 */
	protected function _index_page_filter(&$data)
	{

	}

    protected function _page_filter(&$data){

    }


    /**
     * 编辑资料
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit()
    {
    	$user = session('admin_user')['username'];

    	$id =intval($this->request->param('id'));

    	$rs =Db::table('System_User')->where('id',$id)->find();

    	if($rs['username'] != $user){
    		$this->error('没有权限修改！' );
    	}

        $this->applyCsrfToken();
        $this->_form($this->table, 'form');
    }

    /**
     * 修改密码
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function pass()
    {
        $this->applyCsrfToken();
        if ($this->request->isGet()) {
            $this->verify = false;
            $this->_form($this->table, 'pass');
        } else {
            $post = $this->request->post();
            if ($post['password'] !== $post['repassword']) {
                $this->error('两次输入的密码不一致！');
            }
            $result = NodeService::checkpwd($post['password']);
            if (empty($result['code'])) $this->error($result['msg']);
            if (Data::save($this->table, ['id' => $post['id'], 'password' => md5($post['password'])], 'id')) {
                $this->success('密码修改成功，下次请使用新密码登录！', '');
            } else {
                $this->error('密码修改失败，请稍候再试！');
            }
        }
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _form_filter(&$data)
    {
    	if ($this->request->isPost()) {
    	}
    }




    public function upload()
    {
        if($this->request->file('file')){
            $file       = $this->uploadfile('file','' ,[] , session('admin_user')['id']);
            if ($file) {
                echo json_encode($file);
            } else {
                echo json_encode($file);
            }
        }
    }


    public function uploadfile($name, $path = '', $validate = [], $user_id = 0)
    {
        $config = config('attchment');
        $file   = request()->file($name);
        if ($file) {
            $file_path = $config['path'] . $path;
            $file_url  = $config['url'] . $path;
            $validate  = array_merge($config['validate'], $validate);
            $info      = $file->validate($validate)->move($file_path);
            if ($info) {
                $file_info = [
                	'code'          => 0 ,
                    'user_id'       => $user_id,
                    'original_name' => $info->getInfo('name'),
                    'save_name'     => $info->getFilename(),
                    'save_path'     => str_replace("\\", "/",$file_path . $info->getSaveName()),
                    'extension'     => $info->getExtension(),
                    'mime'          => $info->getInfo('type'),
                    'size'          => $info->getSize(),
                    'md5'           => $info->hash('md5'),
                    'sha1'          => $info->hash(),
                    'url'           => str_replace("\\", "/",$file_url . $info->getSaveName())
                ];
                return $file_info;
            } else {
                return $file->getError();
            }
        } else {
            return '无法获取文件';
        }
        return false;
    }


}
