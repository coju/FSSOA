<?php

// +----------------------------------------------------------------------
// 我的日程
// +----------------------------------------------------------------------

namespace app\person\controller;
error_reporting(0);
use library\Controller;
use think\Db;


/**
 * 我的日程
 */
class Calendar extends Controller
{

    /**
     * 绑定当前数据表
     * @var string
     */
    protected $table = 'Calendar';

    /**
     * 日程首页
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index()
    {
        $this->title = '我的日程';
        $user = session('admin_user')['nickname'];

       	$this->fetch();
    }

    /**
     * 日程列表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
	function getJson()
	{
        $user = session('admin_user')['nickname'];
        $id = session('admin_user')['id'];

        $rs =Db::table('Calendar')->where(" allusers LIKE '%".$user."%' ")->select();
        $data =[];    
		foreach($rs as $k=>$row){
			$allday = $row['allday'];
			$is_allday = $allday==1?true:false;
			
			$data[] = array(
				'id' => $row['id'],
				'title' => $row['title'].'('.$row['nickname'].')',
				'uid' => $row['uid'],
				'nickname' => $row['nickname'],
				'start' => date('Y-m-d H:i',$row['starttime']),
				'end' => date('Y-m-d H:i',$row['endtime']),
				//'url' => $row['url'],
				'allDay' => $is_allday,
				'color' => $row['color']
			);
		}
		
		echo json_encode($data);
	}



	/**
	 * 列表数据处理
	 * @param array $data
	 * @throws \Exception
	 */
	protected function filter_data($data){
	}

    protected function _page_filter(&$data){

    }


    /**
     * 点击新增
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add()
    {
    	if ($this->request->isPost()) {

    		list($data1, $string) = [$this->request->post(), []];

    		

			$data['color'] = $data1['color'];
			if($data['color'] ==''){
				$data['color'] ='#2ec770';
			}
			$data['title'] = $data1['event'];
			if($data['title'] == ''){
				$this->error('请输入内容!');	
			}
			$data['uid'] = session('admin_user')['id'];
			$data['nickname'] = session('admin_user')['nickname'];


			$data['starttime'] = strtotime($data1['starttime']);
			$data['endtime'] = strtotime(  $data1['endtime'] );

			if ($data1['sharer'] =='') {
				$data['allusers']   = session('admin_user')['nickname'];
			}else{
				$data['allusers'] = $data1['sharer'].','.session('admin_user')['nickname'];
			}


			$endflag =intval( $data1['isend'] );
			if($endflag =='0'){
				$data["allday"] =1;
				$data['endtime'] ='0';
			}
			
			if($endflag =='1')
			{
				if( (date("d", $data['starttime']) == date("d", $data['endtime']))  && (date("H:m", $data['starttime']) <> date("H:m", $data['endtime']))  )
				{
					$data["allday"] =0;
				}else{
					$data["allday"] =1;
				}
			}

			$ids =Db::name('Calendar')->strict(false)->insertGetId($data);

			//$this->success('保存成功!', '/admin.html#/person/calendar/index.html?spm=m-77-78-93');


			if($ids > 0){
				$res['code'] =1;
				$res['msg'] ='保存成功';
				return $res;
			}

    	}



    	$user = session('admin_user')['username'];

    	list($data1, $string) = [$this->request->get(), []];

		$date = $data1['date'];
		$enddate = $data1['end'];
		
		if($date==$enddate) $enddate='';
		if(empty($enddate) || $enddate ==''){
			$display = 'style="display:none"';
			$enddate = $date;
			$chk = '';
		}else{
			$display = 'style=""';
			$chk = 'checked';
		}
		$enddate = empty($data1['end'])? $date: $data1['end'];

    	$list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
    	$str ='';
    	foreach ($list as &$value) {
    		$str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
    	}

		$this->assign("date" , $date);
		$this->assign("enddate" , $enddate);
		$this->assign("chk" , $chk);
		$this->assign("display" , $display);
		$this->assign('str' , $str);
        $this->applyCsrfToken();
        $this->_form($this->table, 'add');
    }


     /**
     * 编辑日程
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit()
    {
    	$data =$this->request->param();
    	$id =intval($data['id']);



    	$rs =Db::table('Calendar')->where('id',$id)->find();


    	if ($this->request->isPost()) {

    		list($data1, $string) = [$this->request->post(), []];


			$user = session('admin_user')['nickname'];
			if($rs['nickname'] !=$user){
				$this->error('你只能操作你自己的日程');
			}

    		$id =intval($data1['id']);

			$data['color'] = $data1['color'];
			if($data['color'] ==''){
				$data['color'] ='#2ec770';
			}
			$data['title'] = $data1['event'];
			if($data['title'] == ''){
				$this->error('请输入内容!');	
			}
			$data['uid'] = session('admin_user')['id'];
			$data['nickname'] = session('admin_user')['nickname'];


			$data['starttime'] = strtotime($data1['starttime']);
			$data['endtime'] = strtotime(  $data1['endtime'] );

			if ($data1['sharer'] =='') {
				$data['allusers']   = session('admin_user')['nickname'];
			}else{
				$data['allusers'] = $data1['sharer'].','.session('admin_user')['nickname'];
			}


			$endflag =intval( $data1['isend'] );
			if($endflag =='0'){
				$data["allday"] =1;
				$data['endtime'] ='0';
			}
			
			if($endflag =='1')
			{
				if( (date("d", $data['starttime']) == date("d", $data['endtime']))  && (date("H:m", $data['starttime']) <> date("H:m", $data['endtime']))  )
				{
					$data["allday"] =0;
				}else{
					$data["allday"] =1;
				}
			}
			Db::name('Calendar')
			    ->where('id', $id)
			    ->strict(false)
			    ->data($data)
			    ->update();

			//$this->success('编辑成功!', '/admin.html#/person/calendar/index.html?spm=m-77-78-93');


			//if($ids > 0){
				$res['code'] =1;
				$res['msg'] ='保存成功';
				return $res;
			//}

    	}

    	$allusers =$this->showUser($rs['allusers']);
    	

    	$list = Db::name('SystemUser')->field('id,username,nickname')->where('id >10000')->where(['status'=>'1'])->select();
    	$str ='';
    	foreach ($list as &$value) {
    		$str .="{name:'".$value['nickname']."', value: '".$value['nickname']."'},\r\n";
    	}
    	$this->assign("id" , $id);
    	$this->assign("allusers" , $allusers);
    	$this->assign("str" , $str);
    	$this->_form($this->table, 'edit');
    }


    private function showUser($data)
    {
        if($data !=''){
            $tmp1 = explode(',' , $data);
            $str1 ='';
            if(is_array($tmp1)){
                foreach ($tmp1 as $key => &$v) {
                    $str1 .="'".$v."',";
                }

            }else{
                $str1 =$data;
            }
            $str1 =rtrim($str1 , ',');
        }
        return $str1;
    }


    /**
     * 大小缩放
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
	public function resize()
	{
		list($data1, $string) = [$this->request->post(), []];

		$data["id"] =intval($data1['id']);
		$daydiff = (int)$data1['daydiff']*24*60*60;
		$minudiff = (int)$data1['minudiff']*60;
		

		$row  =Db::table('Calendar')->where(['id'=>$data['id'] ])->find();
		if(count($row)==0){
			$res['code'] =0;
			$res['info'] ='日程不存在！';
			return $res;
		}

		$user = session('admin_user')['nickname'];
		if($row['nickname'] !=$user){
			$res['code'] =0;
			$res['info'] ='权限错误，你只能操作你自己的日程！';
			return $res;
		}

		$difftime = $daydiff + $minudiff;
		if($row['endtime']==0){
			$sql = "update `calendar` set endtime=starttime+'$difftime' where id='".$data["id"]."'";
		}else{
			$sql = "update `calendar` set endtime=endtime+'$difftime' where id='".$data["id"]."'";
		}

		Db::execute($sql); 
		return true;
	}

    /**
     * 拖拽大小
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
	public function draglog()
	{
		list($data1, $string) = [$this->request->post(), []];
		$data["id"] =intval($data1['id']);
		$daydiff = (int)$data1['daydiff']*24*60*60;
		$minudiff = (int)$data1['minudiff']*60;
		$allday = $data1['allday'];
		

		$row  =Db::table('Calendar')->where(['id'=>$data['id'] ])->find();
		if(count($row)==0){
			$res['code'] =0;
			$res['info'] ='日程不存在！';
			return $res;
		}

		$user = session('admin_user')['nickname'];
		if($row['nickname'] !=$user){
			$res['code'] =0;
			$res['info'] ='权限错误，你只能操作你自己的日程！';
			return $res;
		}

		$id = $data['id'];
		
		if($allday=="true"){
			if($row['endtime']==0){
				$sql = "update `calendar` set starttime=starttime+'$daydiff' where id='$id'";
			}else{
				$sql = "update `calendar` set starttime=starttime+'$daydiff',endtime=endtime+'$daydiff' where id='$id'";
			}
		}else{
			$difftime = $daydiff + $minudiff;
			if($row['endtime']==0){
				$sql = "update `calendar` set starttime=starttime+'$difftime' where id='$id'";
			}else{
				$sql = "update `calendar` set starttime=starttime+'$difftime',endtime=endtime+'$difftime' where id='$id'";
			}
		}
		Db::execute($sql); 
		return true;
	}



    /**
     * 删除记录
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function delete()
    {
        list($data1, $string) = [$this->request->post(), []];
        

		$data["id"] =intval($data1['id']);
		$row  =Db::table('Calendar')->where(['id'=>$data['id'] ])->find();
		if(count($row)==0){
			//$this->error('日程不存在');
			$res['code'] =0;
			$res['info'] ='日程不存在！';
			return $res;
		}

		$user = session('admin_user')['nickname'];
		if($row['nickname'] !=$user){
			$res['code'] =0;
			$res['info'] ='权限错误，你只能操作你自己的日程！';
			return $res;
		}
		
		$where = ['id' => $this->request->post('id')];
        Db::name('Calendar')->where($where)->delete();

		$res['code'] =0;
		$res['info'] ='成功删除！';
		return $res;

    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function _form_filter(&$data)
    {
    	if ($this->request->isPost()) {
    	}
    }



}
