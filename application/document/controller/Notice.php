<?php

// +----------------------------------------------------------------------
// 单位公告
// +----------------------------------------------------------------------

namespace app\document\controller;
error_reporting(0);
use app\document\service\DataService;
use library\Controller;
use think\Db;
use think\facade\Config;


/**
 * 单位公告
 */
class Notice extends Controller
{

    /**
     * 绑定当前数据表
     * @var string
     */
    protected $table = 'Notice';

    /**
     * 公告列表
     * @auth true
     * @menu true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function index()
    {
    	$allusers = session('admin_user')['nickname'];

        $this->title = '公告列表';
        $this->_query($this->table)->like('title,content,docno')
        	 ->where(" is_deleted=0 AND status=1 AND wenhao=3")
        	 ->equal('status')->order(' status ASC , id DESC')->page();
    }

	/**
	 * 列表数据处理
	 * @param array $data
	 * @throws \Exception
	 */
	protected function _index_page_filter(&$data)
	{

	}

    protected function _page_filter(&$data)
    {

    }



    /**
     * 添加
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function add()
    {
    	$this->title = '发布公告';

    	
    	if ($this->request->isPost()) {

    		list($data1, $string) = [$this->request->post(), []];

            $data1['addtime'] =time();
            
    		if ($data1['docauthor'] =='') {
    			$data1['docauthor'] =session('admin_user.nickname') ;
    		}

            if ($data1['docenddate'] =='') {
                $data1['docenddate'] =date('Y-m-d' ,time()+7*24*3600) ;
            }


    		$data1['wenhao'] =3;

    		$ids =Db::name('Notice')->strict(false)->insertGetId($data1);
    		
    		//插入附件
        	list($post, $datas) = [$this->request->post(), []];
        	if (isset($post['fileurl']) && is_array($post['fileurl'])) {
        		$files =[];
                foreach (array_keys($post['fileurl']) as $key) {
                	array_push($files, [
	                    'docid'     => $ids,
	                    'username'    => $post['docauthor'],
	                    'original_name' => $post['original_name'][$key],
	                    'fileext'   => $post['fileext'][$key],
	                    'filesize'  => $post['filesize'][$key],
	                    'fileurl'   => $post['fileurl'][$key],
	                    'filename'  => $post['filename'][$key],
	                    'md5code'   => $post['md5code'][$key],
	                    'sha1code'  => $post['sha1code'][$key],
	                    'flag'		=> 'notice',
                	]);
                }
                if (!empty($files)) {
                    Db::name('Attachment')->insertAll($files);
                }
        	}

        	$this->success('恭喜, 数据保存成功!', '/admin.html#/document/notice/index.html?spm=m-77-78-80');
    	
    	}

    	$files =[];
    	$docauthor =session('admin_user.nickname');
    	$this->assign('files' , $files);
    	$this->assign('docauthor' , $docauthor);  	//拟稿人、发送人	
        $this->_form($this->table, 'form');
    }


    /**
     * 修改
     * @auth true
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function edit()
    {
        $this->title = '查看公告';
    	$id =intval($this->request->param('id'));

    	#print_r(session('admin_user.nickname'));

    	$rs =Db::table('Notice')->where('id',$id)->find();

    	if ($this->request->isPost()) {

    		list($data1, $string) = [$this->request->post(), []];
	    	if (session('admin_user')['nickname'] !== $data1['docauthor'] ) {
	    		$this->error('权限不足，操作失败' );
	    	}

            if ($data1['docenddate'] =='') {
                $data1['docenddate'] =date('Y-m-d' ,time()+7*24*3600) ;
            }

            
	    	
			Db::name('Notice')
			    ->where('id', $data1['id'])
			    ->strict(false)
			    ->data($data1)
			    ->update();

        	list($post, $datas) = [$this->request->post(), []];
        	if (isset($post['fileurl']) && is_array($post['fileurl'])) {
        		$files =[];
                foreach (array_keys($post['fileurl']) as $key) {
                	array_push($files, [
	                    'docid'     => $data1['id'],
	                    'username'    => $post['docauthor'],
	                    'original_name' => $post['original_name'][$key],
	                    'fileext'   => $post['fileext'][$key],
	                    'filesize'  => $post['filesize'][$key],
	                    'fileurl'   => $post['fileurl'][$key],
	                    'filename'  => $post['filename'][$key],
	                    'md5code'   => $post['md5code'][$key],
	                    'sha1code'  => $post['sha1code'][$key],
	                    'flag'		=> 'notice',
                	]);
                }
                if (!empty($files)) {
                    Db::name('Attachment')->insertAll($files);
                }
        	}

        	$this->success('恭喜, 数据保存成功!', '/admin.html#/document/notice/index.html?spm=m-77-78-80');
    	}

    	$files =Db::table('Attachment')->where(['docid'=>$id, 'flag'=>'notice'])->select();

    	$this->assign('id' , $id);
    	$this->assign('docauthor' , $rs['docauthor']);
    	$this->assign('files' , $files);
    	$this->assign('docauthor' , $rs['docauthor']);
        $this->_form($this->table, 'form');
    }


    /**
     * 文档打印
     * @auth true
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function printdoc()
    {
    	$id =intval($this->request->param('id'));

    	$rs =Db::table('Notice')->where(['id'=>$id])->find();

    	if(!$rs)
    	{
    		$this->error('文档不存在!');
    	}

    	$file_list =Db::table('Attachment')
    			->where( "  docid='".$rs['id']."' AND flag='notice' ")
    			->select();

    	$this->assign('rs' , $rs);
    	$this->assign('file_list' , $file_list);
    	$this->fetch();
    }

    public function doUploadPic()//上传模块
    {

        if($this->request->file('file')){
            $file       = $this->uploadfile('file','' ,[] , session('admin_user')['id']);
            if ($file) {
                echo '{"code":0,"msg":"成功上传","data":{"src":"'.$file['url'].'"}}';
            } else {
                $this->error('error');
            }
        }
    }


    public function savepic()
    {
        if ($this->request->isPost()) {
            list($data1, $string) = [$this->request->post(), []];

            $data1['picdata'] = base64_image_content($data1['picdata'] , Config::get('app.attchment')['path'] );
  
            $res['code'] =0;
            $res['info'] ='保存成功';
            $res['url'] =$data1['picdata'];
            return $res;

        }
    }

    public function uploadBin()
    {
        ob_start(); //打开缓冲区
        header("Content-Transfer-Encoding: binary"); //告诉浏览器，这是二进制文件

        list($data1, $string) = [$this->request->get(), []];

        $streamData = file_get_contents('php://input' ) ? file_get_contents ('php://input') : gzuncompress ($GLOBALS ['HTTP_RAW_POST_DATA']);

        if(empty($streamData)){  
           //$streamData = file_get_contents('php://input');  
           echo ('201|||没有接收到数据流.' );
           die();
        }

        if( !in_array( $data1['ext']  , explode(',' , Config::get('app.attchment')['validate']['ext'])) ){
            echo ('202|||文件格式错误!' );
            die();
        }

        if ($this->request->isPost()) {

            $new_file = Config::get('app.attchment')['path'].date('Ymd',time())."/";
            if(!file_exists($new_file)){
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdirs($new_file, 0700);
            }

            $file = $new_file . time() .'.'.$data1['ext'];
            file_put_contents( $file , $streamData , true);
            echo '200|||'.'/'.$file;
            die();

        }

    }
	
    public function upload()
    {
        if($this->request->file('file')){
            $file       = $this->uploadfile('file','' ,[] , session('admin_user')['id']);
            if ($file) {
                echo json_encode($file);
            } else {
                echo json_encode($file);
            }
        }
    }


    public function uploadfile($name, $path = '', $validate = [], $user_id = 0)
    {
        $config = config('attchment');
        $file   = request()->file($name);
        if ($file) {
            $file_path = $config['path'] . $path;
            $file_url  = $config['url'] . $path;
            $validate  = array_merge($config['validate'], $validate);
            $info      = $file->validate($validate)->move($file_path);
            if ($info) {
                $file_info = [
                	'code'          => 0 ,
                    'user_id'       => $user_id,
                    'original_name' => $info->getInfo('name'),
                    'save_name'     => $info->getFilename(),
                    'save_path'     => str_replace("\\", "/",$file_path . $info->getSaveName()),
                    'extension'     => $info->getExtension(),
                    'mime'          => $info->getInfo('type'),
                    'size'          => $info->getSize(),
                    'md5'           => $info->hash('md5'),
                    'sha1'          => $info->hash(),
                    'url'           => str_replace("\\", "/",$file_url . $info->getSaveName())
                ];
                return $file_info;
            } else {
                return $file->getError();
            }
        } else {
            return '无法获取文件';
        }
        return false;
    }


}
